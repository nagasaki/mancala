#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <iostream>
#include <string>
#include <array>
#include <bitset>
#include"Global.h"

using namespace std;

//リンカ
#pragma comment( lib, "ws2_32.lib" )

typedef struct {
	char Alpha;
	bool PFlag;
}DATA;

bool initClientSock();
void Release();
void displayBoard(array<int, 14>& boardvalue);
short pocketSelect(int& player, int& startpocket, char *alpha);
void moveValue(array<int, 14>& boardvalue,int& player,int& startpocket);
bool gameFinish(array<int, 14>& boardvalue,bool& finished);
void judgeWinner(array<int, 14>& boardvalue,int& player);

//ボード全体の値を保持する配列
array<int, 14> boardvalue{ 0,4,4,4,4,4,4,4,4,4,4,4,4,0 };
SOCKET sendSock,recvSock;
SOCKADDR_IN sendAddr,recvAddr;
int sendAddrlen = sizeof(sendAddr);
int recvAddrlen = sizeof(recvAddr);
bool broadYes = 1;//ブロードキャストをするか

int main()
{
	string Players[] = { "Player 1","Player 2" };
	bool finished = false;//ループが終わるかの判定
	int player = 0;//プレイヤーを初期化


	if (!initClientSock()) { return finished = true; }

	puts("あなたはPlayer1");
	displayBoard(boardvalue);//盤面を表示
	cout << Players[player] << "のターン\n";

	while (!finished)
	{
		DATA recvData, sendData;
		int ret,startpocket,e;
		sendData.PFlag = player;
		//char alpha;//アルファベットの入力

		//pocketSelectにalphaをポインタ渡しで値を変更しつつ送受信をする

		cout << endl << "動かしたい石の位置を指定してください (" << (player == 0 ? " A 〜 F " : " G 〜 L ") << ")";
		//pocketSelect(player, alpha, startpocket);//ポケットの選択
		pocketSelect(player, startpocket, &sendData.Alpha);
		//---------------------Client(Player1)------------------------------------

		moveValue(boardvalue, player,startpocket);//選択された石の移動処理
		gameFinish(boardvalue, finished);//ゲームの終了判定
		system("cls");//画面クリア
		player = !player;//プレイヤーの切り替え
		displayBoard(boardvalue);//移動された盤面の表示
		cout << Players[player] << "のターン\n";

		//ポケットの選択情報の送信
		//第一引数：ソケット
		//第二引数：送信するメッセージが入っているバッファへのポインタ→バッファは送りたいデータを指す。ポインタは先頭アドレスを指す
		//(ベクトルで考えて。始点と長さそのベクトルがわかるように、バッファは始点、ポインタは長さと覚えておこう)
		//第三引数：バッファ内のメッセージの長さ
		//第四引数：フラグの設定は基本０で。OOB(アウトオブバンド)や、dontroute -> パケットの経路を指定せずにデータを送信する
		//第五引数：送信先のアドレス
		//第六引数：第五引数が指すアドレスのサイズ
		ret = sendto(sendSock, (char *)&sendData, sizeof(sendData), 0, (SOCKADDR*)&sendAddr, sendAddrlen);
		if (ret < 0)
		{
			puts("Error:sendto");
			e = WSAGetLastError();
		}
		//puts("Success:sendto");
		//------------------------Server(Player2)--------------------------------------------
		//ポケットの選択情報の受信
		//第一引数：ソケット
		//第二引数：データを受け取るバッファへのポインタ
		//第三引数：バッファの長さ(バイト単位)
		//第四引数：フラグ設定　
		//MSG_CONNTERM,MSG_OOB,MSG_PEEK,MSG_WAITALLがある。後で確認
		//第五引数：データを受信するソケット・アドレス構造体へのポインタ。第五引数が0以外の場合、送信元アドレスが戻される
		//第六引数：第五引数が示すストレージのサイズ

		while (player != sendData.PFlag)
		{
			ret = recvfrom(recvSock, (char *)&recvData, sizeof(recvData), 0, nullptr, nullptr);
			if (ret < 0)
			{
				puts("Error:recvfrom");
				e = WSAGetLastError();
			}
			//puts("Success:recvfrom");
			recvData.Alpha = toupper(recvData.Alpha);//小文字を大文字に変換

			startpocket = (int)(recvData.Alpha - 64);//ASCIIコードを配列の要素番号に合わせる
			moveValue(boardvalue, player, startpocket);//選択された石の移動処理
			//gameFinish(boardvalue, finished);//ゲームの終了判定
			system("cls");//画面クリア
			player = !player;//プレイヤーの切り替え
			displayBoard(boardvalue);//移動された盤面の表示
			cout << Players[player] << "のターン\n";
			if (gameFinish(boardvalue, finished)) { break; }
		}
		if (gameFinish(boardvalue, finished)) { break; }
	}
	judgeWinner(boardvalue, player);
	Release();
	system("pause");
	return 0;
}

//自分の手番が終了時にデータを送っていけばいい

bool initClientSock()
{
	int setsock,ret,e;

	//WinSock2.2の初期化
	WSADATA wsaData;
	//引数1：使いたいWinSockのバージョン指定
	//引数2：WinSock の情報を得るためにWSADATA 構造体へのポインタを指定
	//戻値：成功→0,失敗→-1
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) == SOCKET_ERROR)
	{
		puts("Error:WSAStartup");
		return false;
	}
	puts("Success:WSAStartup");

	//UDPソケットの作成
	//引数1：アドレス・ドメイン→AF_INET(IPv4)
	//引数2：ソケットタイプ(通信方式)→SOCK_DGRAM(UDP)
	//引数3：ソケットが使用するプロトコルの指定→IPPROTO_UDP(UDPだけ使うよ！)
	//戻値：成功→0,失敗→-1
	sendSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sendSock < 0)
	{
		puts("Error:sendSock");
		return false;
	}
	puts("Success:sendSock");

	recvSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (recvSock < 0)
	{
		puts("Error:recvSock");
		return false;
	}
	puts("Success:recvSock");
	
	//ソケット(ブロードキャスト)の設定
	//引数１：socket
	//引数２：オプションが定義されるレベル
	//引数３：ソケットオプション
	//引数４：オプションのための値が指定されるバッファへのポインタ
	//引数５：第四引数のバッファサイズ
	//戻値：成功：０、失敗：SOCKET_ERROR
	setsock = setsockopt(sendSock, SOL_SOCKET, SO_BROADCAST, (char *)&broadYes, sizeof(broadYes));
	if (setsock == SOCKET_ERROR)
	{
		puts("Error:setsock");
		e = WSAGetLastError();
		cout << e << endl;
		WSACleanup();
		closesocket(sendSock);
		return false;
	}
	puts("Success:setsock");

	//送信先設定
	sendAddr.sin_family = AF_INET;//アドレスファミリ(ネットワークのアドレスの種類)
	sendAddr.sin_port = htons(SERVER_PORT_25000);//ポート番号
	sendAddr.sin_addr.S_un.S_addr = inet_addr(DEFAULT_ADDERESS);//IPアドレス
	//

	//ADDRESSを送る
	int  dummy = 0;
	ret = sendto(sendSock, (char*)&dummy, sizeof(dummy), 0, (SOCKADDR*)&sendAddr, sendAddrlen);
	if (ret < 0)
	{
		puts("Error:sendto");
		e = WSAGetLastError();
		cout << e << endl;
		WSACleanup();
		closesocket(sendSock);
		return false;
	}
	puts("Success:sendto");

	//ローカルで受信用のアドレス格納用を作成してbindが終わったら削除
	recvAddr.sin_family = AF_INET;
	recvAddr.sin_port = htons(SERVER_PORT_25001);
	recvAddr.sin_addr.S_un.S_addr = INADDR_ANY;

	//ソケットの登録
	//引数1：作成したソケットの指定
	//引数2：ソケットに割り当てるポインタアドレス
	//引数3：引数2が指す構造体のサイズ
	//戻値：成功→0,失敗→-1
	if (bind(recvSock, (SOCKADDR*)&recvAddr, recvAddrlen))
	{
		puts("Error:bind");
		closesocket(recvSock);
		closesocket(sendSock);
		WSACleanup();
		return false;
	}
	puts("Success:bind");

	puts("ホストを探しています　しばらくお待ちください");
	ret = recvfrom(recvSock, (char*)&dummy, sizeof(dummy), 0, (SOCKADDR*)&recvAddr, &recvAddrlen);
	if (ret < 0)
	{
		puts("Error:recvfrom");
		e = WSAGetLastError();
		cout << e << endl;
		WSACleanup();
		closesocket(recvSock);
		closesocket(sendSock);
		return false;
	}
	puts("Success:recvfrom");
	system("cls");
	return true;
}

//表示
void displayBoard(array<int, 14>& boardvalue)
{
	cout << endl << "\tA\tB\tC\tD\tE\tF\n\t";//\t->水平タブ
	for (int i = 1; i <= 6; i++) { cout << boardvalue[i] << "\t"; }
	cout << endl << " " << boardvalue[GOALPOKECTLEFT] << "\t\t\t\t\t\t\t" << boardvalue[GOALPOKECTRIGHT] << endl << "\t";
	for (int i = 7; i <= 12; i++) { cout << boardvalue[i] << "\t"; }
	cout << endl << "\tG\tH\tI\tJ\tK\tL";
	cout << endl << endl << endl;
}

//ポケットの選択
short pocketSelect(int& player, int& startpocket,char* alpha)
{
	bool input_ok = false;//入力の合否

	//Playerの入力
	while (!input_ok)
	{
		cin >> *alpha;
		*alpha = toupper(*alpha);//小文字を大文字に変換
		startpocket = (int)(*alpha - 64);//ASCIIコードを配列の要素番号に合わせる

		//A〜F入力された
		if (*alpha >= 'A' && *alpha <= 'F')
		{
			//Player1だったら
			if (player == 0) { input_ok = true; }
			else
			{
				puts("自陣のポケットを指定してください");
				cout << endl << "もう一度動かしたい石の位置を指定してください (" << (player == 0 ? " A 〜 F " : " G 〜 L ") << ")";
				continue;
			}

		}
		//G〜L入力された
		else if (*alpha >= 'G' && *alpha <= 'L')
		{
			//Player2だったら
			if (player == 1) { input_ok = true; }
			else 
			{
				puts("自陣のポケットを指定してください");
				cout << endl << "もう一度動かしたい石の位置を指定してください (" << (player == 0 ? " A 〜 F " : " G 〜 L ") << ")";
				continue;
			}
		}
		else 
		{
			puts("そのような場所は存在しません");
			cout << endl << "もう一度動かしたい石の位置を指定してください (" << (player == 0 ? " A 〜 F " : " G 〜 L ") << ")";
			continue;
		}

		//選択したとこに何も入っていなかったら
		if (boardvalue[startpocket] == 0)
		{
			puts("そこに石は入っていません");
			cout << endl << "もう一度動かしたい石の位置を指定してください (" << (player == 0 ? " A 〜 F " : " G 〜 L ") << ")";
			input_ok = false;
		}
		else
			return startpocket;
	}
}
void Release()
{
	// 終了処理
	closesocket(recvSock);
	closesocket(sendSock);
	WSACleanup();
}

//ポケットの移動
void moveValue(array<int, 14>& boardvalue, int& player, int& startpocket)
{

	int countstone = 0;//持っている個数と置いた個数の確認用
	int movestones, pocket;

	//反時計回りにポケットを移動
	pocket = startpocket;
	//選択したボードの中にある石の数を確保
	movestones = boardvalue[pocket];
	//movestonesで確保したため(手に持っている想定)ボードの値を０に
	boardvalue[pocket] = 0;

	//持っている石の数だけループ
	while (countstone < movestones)
	{
		//選択されたポケットの識別
		if (pocket >= 0 && pocket <= 6)
		{
			//ポケットがゴールポケットだった場合
			if (pocket == GOALPOKECTLEFT)
			{
				pocket = 7;//ポケットの変更
				boardvalue[pocket]++;//要素に追加
				countstone++;//カウントを進める
				continue;
			}
			else
			{
				//Player2の時 && pocketの位置が相手ゴールポケットの一個前だった時
				if (player == 1 && pocket == 1)
				{
					pocket = GOALPOKECTLEFT;
					continue;
				}
				else
				{
					pocket--;//pocketの移動
					boardvalue[pocket]++;//移動したポケットに石を追加
					countstone++;//ループカウントを進める
				}
			}
		}
		else if (pocket >= 7 && pocket <= 13)
		{
			if (pocket == GOALPOKECTRIGHT)
			{
				pocket = 6;
				boardvalue[pocket]++;
				countstone++;
				continue;
			}
			else
			{
				if (player == 0 && pocket == 12)
				{
					pocket = GOALPOKECTRIGHT;
					continue;
				}
				else
				{
					pocket++;
					boardvalue[pocket]++;
					countstone++;
				}
			}
		}
	}

	if (pocket == GOALPOKECTLEFT && player == 0 && countstone == movestones)
	{
		player = !player;
	}
	else if (pocket == GOALPOKECTRIGHT && player == 1 && countstone == movestones)
	{
		player = !player;
	}
}
//ゲームが終わったか
bool gameFinish(array<int, 14>& boardvalue, bool& finished)
{
	bitset<6> bs1("111111");
	bitset<6> bs2("111111");
	//Player1のボードの値が全部ゼロだったら
	for (int i = 1; i < 7; i++)
	{
		if (boardvalue[i] > 0)
		{
			bs1[i - 1] = false;
		}
	}

	//Player2のボードの値が全部ゼロだったら
	for (int i = 7; i < 13; i++)
	{
		//A〜FまたはG〜Lのポケットに石が入っているとき
		if (boardvalue[i] > 0)
		{
			bs2[i - 7] = false;
		}
	}
	if (bs1.all() || bs2.all())
	{
		finished = true;
		return finished;
	}
	else return finished;

}
//勝利判定
void judgeWinner(array<int, 14>& boardvalue, int& player)
{
	for (int i = 1; i < 7; i++)
	{
		boardvalue[GOALPOKECTLEFT] += boardvalue[i];
		boardvalue[i] = 0;
	}

	for (int j = 8; j < 13; j++)
	{
		boardvalue[GOALPOKECTRIGHT] += boardvalue[j];
		boardvalue[j] = 0;
	}

	system("cls");
	displayBoard(boardvalue);//盤面を表示

	if (boardvalue[GOALPOKECTLEFT] > boardvalue[GOALPOKECTRIGHT])
	{
		puts("あなたの勝利！おめでとう！");
	}
	else if (boardvalue[GOALPOKECTLEFT] < boardvalue[GOALPOKECTRIGHT])
	{
		puts("あなたの負け。次こそは勝つ！");
	}
	else puts("引き分けだよ！仲がいいね！");
}
#pragma once
#include <WinSock2.h>

#define _WINSOCK_DEPRECATED_NO_WARNINGS

#define DEFAULT_ADDERESS "192.168.42.255"

//ポート番号
#define SERVER_PORT_25000 25000
#define SERVER_PORT_25001 25001


//マンカラのゴールポケット位置
#define GOALPOKECTLEFT 0
#define GOALPOKECTRIGHT 13

#include <iostream>
#include <WinSock2.h>

#pragma comment( lib, "ws2_32.lib" )

using namespace std;

typedef struct {
	u_int vPos;
	u_int hPos;
}CELL;

enum CELLSTATE{
	STATE_EMPTY,
	STATE_BLACK,
	STATE_WHITE
};

const u_int CELLCOUNT = 8;
const u_short SERVERPORT = 8000;
const char* CELLSTRING[] = { "  ", "��", "��" };

SOCKET sock;
SOCKADDR_IN enemySockAddr;

CELLSTATE stage[CELLCOUNT][CELLCOUNT];

bool Init();
void Draw();

void InitStage();
bool InitWinSock();

int main()
{
	if( !Init() )
	{
		return -1;
	}
	cout << "Init()\n";

	Draw();
	while( true )
	{
		CELL recvCell, sendCell;
		int addrlen = sizeof(enemySockAddr);
		int ret;
		int vPos, hPos;

		
		// ��M
		ret = recvfrom(sock, (char *)&recvCell, sizeof(recvCell), 0, (SOCKADDR*)&enemySockAddr, &addrlen);
		vPos = ntohl( recvCell.vPos );
		hPos = ntohl( recvCell.hPos );
		stage[vPos][hPos] = STATE_BLACK;

		// �`��
		Draw();

		// �I��
		cout << "input v:";
		cin >> vPos;

		cout << "input h:";
		cin >> hPos;

		stage[vPos][hPos] = STATE_WHITE;

		sendCell.vPos = htonl( vPos );
		sendCell.hPos = htonl( hPos );

		// ���M
		sendto(sock, (char *)&sendCell, sizeof(sendCell), 0, (SOCKADDR*)&enemySockAddr, addrlen);

		// �`��
		Draw();
	}

	system("pause");
	return 0;
}

bool Init()
{
	InitStage();
	if( !InitWinSock() )
	{
		return false;
	}

	sock = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );
	if( sock < 0 )
	{
		return false;
	}

	SOCKADDR_IN bindAddr;
	memset( &bindAddr, 0, sizeof( bindAddr ) );
	bindAddr.sin_family = AF_INET;
	bindAddr.sin_port = htons( SERVERPORT );
	bindAddr.sin_addr.s_addr = INADDR_ANY;


	if( bind( sock, (SOCKADDR *)&bindAddr, sizeof( bindAddr ) ) < 0 )
	{
		return false;
	}

	return true;
}

void InitStage()
{
	for(u_int v = 0; v < CELLCOUNT; v++)
	{
		for(u_int h = 0; h < CELLCOUNT; h++)
		{
			stage[v][h] = STATE_EMPTY;
		}
	}

	stage[3][3] = STATE_BLACK;
	stage[4][4] = STATE_BLACK;
	stage[3][4] = STATE_WHITE;
	stage[4][3] = STATE_WHITE;
}

bool InitWinSock()
{
	WSADATA wsaData;

	return ( WSAStartup( MAKEWORD( 2, 2 ), &wsaData ) == 0 );
}

void Draw()
{
	cout << "-------------------------\n";
	for( u_int v = 0; v < CELLCOUNT; v++ )
	{
		cout << "|";
		for( u_int h = 0; h < CELLCOUNT; h++ )
		{
			cout << CELLSTRING[stage[v][h]] << "|";
		}
		cout << "\n";
	}

	cout << "-------------------------\n";
}

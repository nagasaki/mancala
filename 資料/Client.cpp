#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <iostream>
#include <WinSock2.h>

#pragma comment( lib, "ws2_32.lib" )

using namespace std;

typedef struct {
	u_int vPos;
	u_int hPos;
}CELL;

enum CELLSTATE{
	STATE_EMPTY,
	STATE_BLACK,
	STATE_WHITE
};

const u_int CELLCOUNT = 8;
const u_short SERVERPORT = 8000;
const char* SERVERIPADDR = "127.0.0.1";	// サーバのIPアドレス
const char* CELLSTRING[] = {"  ", "●", "○"};

SOCKET sock;
SOCKADDR_IN enemySockAddr;

CELLSTATE stage[CELLCOUNT][CELLCOUNT];

bool Init();
void Draw();

void InitStage();
bool InitWinSock();

int main()
{
	if(!Init())
	{
		return -1;
	}
	cout << "Init()\n";

	Draw();
	while(true)
	{
		CELL recvCell, sendCell;
		int addrlen = sizeof(enemySockAddr);
		int ret;
		int vPos, hPos;

		// 選択
		cout << "input v:";
		cin >> vPos;

		cout << "input h:";
		cin >> hPos;

		stage[vPos][hPos] = STATE_BLACK;

		sendCell.vPos = htonl(vPos);
		sendCell.hPos = htonl(hPos);

		// 送信
		ret = sendto( sock, (char *)&sendCell, sizeof(sendCell), 0, (SOCKADDR*)&enemySockAddr, addrlen);

		// 描画
		Draw();

		// 受信
		ret = recvfrom( sock, (char *)&recvCell, sizeof(recvCell), 0, (SOCKADDR*)&enemySockAddr, &addrlen);
		
		vPos = ntohl(recvCell.vPos);
		hPos = ntohl(recvCell.hPos);

		stage[vPos][hPos] = STATE_WHITE;

		// 描画
		Draw();
	}

	system("pause");
	return 0;
}

bool Init()
{
	InitStage();
	if(!InitWinSock())
	{
		return false;
	}

	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sock < 0)
	{
		return false;
	}

	//SOCKADDR_IN bindAddr;
	memset(&enemySockAddr, 0, sizeof(enemySockAddr));
	enemySockAddr.sin_family = AF_INET;
	enemySockAddr.sin_port = htons(SERVERPORT);
	enemySockAddr.sin_addr.s_addr = inet_addr( SERVERIPADDR );


	return true;
}

void InitStage()
{
	for(u_int v = 0; v < CELLCOUNT; v++)
	{
		for(u_int h = 0; h < CELLCOUNT; h++)
		{
			stage[v][h] = STATE_EMPTY;
		}
	}

	stage[3][3] = STATE_BLACK;
	stage[4][4] = STATE_BLACK;
	stage[3][4] = STATE_WHITE;
	stage[4][3] = STATE_WHITE;
}

bool InitWinSock()
{
	WSADATA wsaData;

	return (WSAStartup(MAKEWORD(2, 2), &wsaData) == 0);
}

void Draw()
{
	cout << "-------------------------\n";
	for(u_int v = 0; v < CELLCOUNT; v++)
	{
		cout << "|";
		for(u_int h = 0; h < CELLCOUNT; h++)
		{
			cout << CELLSTRING[stage[v][h]] << "|";
		}
		cout << "\n";
	}
	cout << "-------------------------\n";
}

#include <iostream>
#include <string>
#include <array>
#include <bitset>

#define GOALPOKECTLEFT 0
#define GOALPOKECTRIGHT 13
using namespace std;

//ポケットに値を保持、ゴールポケットに値を入れていく
void displayBoard(array<int, 14>& boardvalue);
void moveValue(array<int, 14>& boardvalue, int& player);
bool gameFinish(array<int, 14>& boardvalue, bool& finished);
void judgeWinner(array<int, 14>& boardvalue, int& player);

//ボード全体の値を保持する配列
//boardvalue[0]->左ゴールポケット、boardvalue[7]->右ゴールポケット
array<int, 14> boardvalue{ 0,4,4,4,4,4,4,4,4,4,4,4,4,0 };
int main()
{
	string Players[] = { "Player 1","Player 2" };
	bool finished = false;//ループが終わるかの判定
	int player = 0;//プレイヤーを初期化
	displayBoard(boardvalue);//盤面を表示
	cout << Players[player] << "のターン\n";

	//trueになったら終了
	while (!finished)
	{
		//選択された石の移動処理
		moveValue(boardvalue, player);
		gameFinish(boardvalue, finished);//ゲームの終了判定
		system("cls");//画面クリア
		player = !player;//プレイヤーの切り替え
		displayBoard(boardvalue);//移動された盤面の表示
		cout << Players[player] << "のターン\n";
	}
	judgeWinner(boardvalue, player);
}


void displayBoard(array<int, 14>& boardvalue)
{
	cout << endl << "\tA\tB\tC\tD\tE\tF\n\t";//\t->水平タブ
	for (int i = 1; i <= 6; i++) { cout << boardvalue[i] << "\t"; }
	cout << endl << " " << boardvalue[GOALPOKECTLEFT] << "\t\t\t\t\t\t\t" << boardvalue[GOALPOKECTRIGHT] << endl << "\t";
	for (int i = 7; i <= 12; i++) { cout << boardvalue[i] << "\t"; }
	cout << endl << "\tG\tH\tI\tJ\tK\tL";
	cout << endl << endl << endl;
}

void moveValue(array<int, 14>& boardvalue, int &player)
{

	bool input_ok;//入力の合否
	char alpha;//アルファベットの入力
	int countstone = 0;//持っている個数と置いた個数の確認用
	int movestones, pocket, start_pocket;

	//Playerの入力
	do
	{
		input_ok = false;
		cout << endl << "動かしたい石の位置を指定してください (" << (player == 0 ? " A 〜 F " : " G 〜 L ") << ")";

		cin >> alpha;
		alpha = toupper(alpha);//小文字を大文字に変換
		start_pocket = (int)(alpha - 64);//ASCIIコードを配列の要素番号に合わせる
		//{A〜Fが選択されたとき　&&　Player1の場合　&&　選択された石が０以上｝|| {G〜Lが選択されたとき　&&　Player2の場合　&&　選択された石が０以上｝なら
		if (((alpha >= 'A' && alpha <= 'F') && player == 0) && boardvalue[start_pocket] > 0 || ((alpha >= 'G' && alpha <= 'L') && player == 1) && boardvalue[start_pocket] > 0) 
		{
			input_ok = true; 
		}
		else if (((alpha >= 'A'&&alpha <= 'F') && player == 1) || ((alpha >= 'G' && alpha <= 'L') && player == 0)) 
		{
			cout << "自陣のポケットを指定してください" << endl;
		}
		else if (alpha > 'L')
		{
			cout << "そのような場所は存在しません" << endl;
		}
		else cout << endl << "そこに石は入っていません" << endl;

	} while (!input_ok);//trueではないときループ

	
	//if (start_pocket <= 6)
	//{
	//	//反時計回りにポケットを移動
	//	pocket = start_pocket;
	//	//選択したボードの中にある石の数を確保
	//	movestones = boardvalue[pocket];
	//	//movestonesで確保したため(手に持っている想定)ボードの値を０に
	//	boardvalue[pocket] = 0;
	//}
	//else if (start_pocket >= 7)
	//{
	//	pocket = start_pocket;
	//	movestones = boardvalue[pocket];
	//	boardvalue[pocket] = 0;
	//}

	//反時計回りにポケットを移動
	pocket = start_pocket;
	//選択したボードの中にある石の数を確保
	movestones = boardvalue[pocket];
	//movestonesで確保したため(手に持っている想定)ボードの値を０に
	boardvalue[pocket] = 0;

	//持っている石の数だけループ
	while(countstone < movestones)
	{
		//選択されたポケットの識別
		if (pocket >= 0 && pocket <= 6)
		{
			//ポケットがゴールポケットだった場合
			if (pocket == GOALPOKECTLEFT)
			{
				pocket = 7;//ポケットの変更
				boardvalue[pocket]++;//要素に追加
				countstone++;//カウントを進める
				continue;
			}
			else
			{
				//Player2の時 && pocketの位置が相手ゴールポケットの一個前だった時
				if (player == 1 && pocket == 1)
				{
					pocket = GOALPOKECTLEFT;
					continue;
				}
				else
				{
				pocket--;//pocketの移動
				boardvalue[pocket]++;//移動したポケットに石を追加
				countstone++;//ループカウントを進める
				}
			}
		}
		else if (pocket >= 7 && pocket <= 13)
		{
			if (pocket == GOALPOKECTRIGHT)
			{
				pocket = 6;
				boardvalue[pocket]++;
				countstone++;
				continue;
			}
			else
			{
				if (player == 0 && pocket == 12)
				{
					pocket = GOALPOKECTRIGHT;
					continue;
				}
				else
				{
				pocket++;
				boardvalue[pocket]++;
				countstone++;
				}
			}
		}
	}

	if (pocket == GOALPOKECTLEFT && player == 0 && countstone == movestones)
	{
		player = !player;
	}
	else if (pocket == GOALPOKECTRIGHT && player == 1 && countstone == movestones)
	{
		player = !player;
	}

}


bool gameFinish(array<int, 14>& boardvalue, bool& finished)
{
	bitset<6> bs1("111111");
	bitset<6> bs2("111111");
	//Player1のボードの値が全部ゼロだったら
	for (int i = 1; i < 7; i++)
	{
		if (boardvalue[i] > 0)
		{
			bs1[i - 1] = false;
		}
	}

	//Player2のボードの値が全部ゼロだったら
	for (int i = 7; i < 13; i++)
	{
		//A〜FまたはG〜Lのポケットに石が入っているとき
		if (boardvalue[i] > 0)
		{
			bs2[i - 7] = false;
		}
	}
	if (bs1.all() || bs2.all())
	{
		finished = true;
		return finished;
	}
	else return finished;

}

void judgeWinner(array<int, 14>& boardvalue,int& player)
{
	for (int i = 1; i < 7; i++)
	{
		boardvalue[GOALPOKECTLEFT] += boardvalue[i];
		boardvalue[i] = 0;
	}

	for (int j = 8; j < 13; j++)
	{
		boardvalue[GOALPOKECTRIGHT] += boardvalue[j];
		boardvalue[j] = 0;
	}

	system("cls");
	displayBoard(boardvalue);//盤面を表示

	if (boardvalue[GOALPOKECTLEFT] > boardvalue[GOALPOKECTRIGHT])
	{
		cout << "Player1の勝利！おめでとう！\n";
	}
	else if (boardvalue[GOALPOKECTLEFT] < boardvalue[GOALPOKECTRIGHT])
	{
		cout << "Player2の勝利！やったね！\n";
	}
	else cout << "引き分けだよ！仲がいいね！\n";
}
//
//どちらのポケットがなくなったのかを調べる必要がある
//↑まずはどっちのゴールポケットに追加しないといけないのか
//→gamefinishにあるbitsetでどっちに追加しないといけないのかがわかる
//追加する方にsumを使って石を足す
//お互いのゴールポケットの石の数を比較して大きい方が勝ちとする

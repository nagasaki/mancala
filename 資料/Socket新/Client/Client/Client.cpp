//インクルード
#include "Global.h"


//名前空間
using namespace WinSock;


//送信データ(キャラクタ構造体)
struct	CharaStatus
{
	//メンバ
	char  charaName[64] = {"勇者　ああああ"};
	u_int charaHp		= htonl(25);
	u_int charaMp		= htonl(20);
	u_int charaAttack	= htonl(6);
	u_int charaDefence	= htonl(4);
	bool  alive			= true;
}character_;


//main
int main()
{
	//WinSock初期化
	if (Initialize(CLIENT) != SUCCESS)return FAILURE;

	//送信
	if (OutputData(UDP, character_, datas[SERVER_DATA]) == F_UDPSEND)return FAILURE;

	int ret1;
	int cnt = 4;
	while (cnt--)
	{
		Sleep(1000);

		//受信
		ret1 = InputData(UDP, VARIABLE(character_));
		if (ret1 != F_UDPRECV && ret1 != WSAEWOULDBLOCK)
		{
			//送信
			if (OutputData(UDP, character_, datas[ret1]) == F_UDPSEND)return FAILURE;
		}
	}

	//終了処理
	if (Release() == F_RELEASE)return FAILURE;
	system("pause");
	return 0;
}
//インクルードガード
#pragma once
#ifndef GLOBAL_H
#define GLOBAL_H
#endif // !GLOBAL_H


//インクルード
#include "Error.h"


//ディファイン
//エラー回避
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS

//サーバ情報格納場所
#define SERVER_DATA	0

//クライアントとサーバ
#define SERVER		WinSock::S_TYPE
#define CLIENT		WinSock::C_TYPE

//通信タイプ
#define UDP			0
#define TCP			1

//受信時データを保存
#define SAVE		1

//変数の型名と変数名を取得
#define GET_VARIABLE_MOLD(v)	typeid(v).name()
#define NAME(v)					#v
#define VARIABLE(v)				&v,GET_VARIABLE_MOLD(v), NAME(v)


//インクルード
#include <ctime>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <WinSock2.h>
#include <Windows.h>


//リンカ
#pragma comment( lib, "ws2_32.lib" )


//ソケットに使われるデータを管理
namespace WinSock
{
	//サーバorクライアント
	enum SorCTYPE
	{
		S_TYPE,
		C_TYPE
	};

	//ブロッキングモード
	enum BLOCKING_MODE
	{
		BLOCK,
		NON_BLOCK,
	};

	//IPアドレス・ポート情報一覧
	struct INFO
	{
		ULONG					IP_ADDR;
		USHORT					PORT;
	};

	//DATAベクター
	extern std::vector<INFO>	datas;

	//WinSock初期化
	int Initialize(SorCTYPE type);

	//ブロッキングモードに設定
	int SetBlockingMode();

	//ノンブロッキングモードに設定
	int SetNonBlockingMode();

	//受信(UDP)
	int RecvUDP(char* recvData, int size, const char* variableMold, const char* variableName);

	//受信(TCP)
	int RecvTCP(char* recvData, int size, const char* variableMold, const char* variableName);

	//受信
	template<class data>
	int InputData(bool type, data* recvData, std::string variableMold, std::string variableName)
	{
		//変数の型名
		int find = variableMold.find(" *");
		variableMold = variableMold.substr(0, find);

		switch (type)
		{
		case UDP:
			return RecvUDP((char*)recvData, sizeof(*recvData), variableMold.c_str(), variableName.c_str());
		case TCP:
			return RecvTCP((char*)recvData, sizeof(*recvData), variableMold.c_str(), variableName.c_str());
		}
		return FAILURE;
	}

	//送信先設定
	void SetSendAddr(INFO info);

	//送信(UDP)
	int SendUDP(char* sendData, int size);

	//送信(TCP)
	int SendTCP(char* sendData, int size);

	//送信
	template<class data>
	int OutputData(bool type, data sendData, INFO info)
	{
		//送信先設定
		SetSendAddr(info);

		switch (type)
		{
		case UDP:
			return SendUDP((char*)&sendData, sizeof(sendData));
		case TCP:
			return SendTCP((char*)&sendData, sizeof(sendData));
		}
		return FAILURE;
	}

	//解放
	int Release();
}
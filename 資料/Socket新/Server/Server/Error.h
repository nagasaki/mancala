//インクルードガード
#pragma once
#ifndef ERROR_H
#define ERROR_H
#endif // !ERROR_H


//成功
#define		SUCCESS				  0		//成功

//失敗
#define		FAILURE				 -1		//失敗
#define		NOT_EXISTS			 -2		//サーバでもクライアントでもない
#define		F_WSASTARTUP		 -1		//WinSockの初期化に失敗
#define		F_UDPSOCK			 -2		//UDPソケットの作成に失敗
#define		F_TCPSOCK			 -3		//TCPソケットの作成に失敗
#define		F_UDPBIND			 -4		//UDPのバインドに失敗
#define		F_TCPBIND			 -5		//TCPのバインドに失敗
#define		F_BLOCK				 -6		//ブロッキングモードの設定に失敗
#define		F_NONBLOCK			 -7		//ノンブロッキングモードの設定に失敗
#define		F_UDPRECV			 -8		//UDP受信に失敗
#define		F_TCPRECV			 -9		//TCP受信に失敗
#define		F_UDPSEND			-10		//UDP送信に失敗
#define		F_TCPSEND			-11		//TCP送信に失敗
#define		F_CLOSE				-12		//ソケットの解放に失敗
#define		F_RELEASE			-13		//WinSockの解放に失敗

//インクルード
#include "Global.h"


//ソケットに使われるデータを管理
namespace WinSock
{
	//サーバorクライアント
	SorCTYPE			ownerType = SERVER;

	//DATAベクター
	std::vector<INFO>	datas;
	int AddAddr(ULONG recvAddr, USHORT recvPort);

	//サーバのポート番号
	const USHORT		SERVER_PORT = 8000;

	//WinSock初期化
	WSADATA				wsaData;
	bool				winSockInitFlg = false;

	//フォルダ作成とファイルの初期化フラグ
	bool				fileFlg = false;

	//ブロッキング情報
	ULONG				arg = BLOCK;

	//ソケット
	SOCKET				UDPSock = -1;
	SOCKET				TCPSock = -1;
	int MakeSocket();

	//受信設定
	sockaddr_in			bindAddr;
	int SetBindAddr();

	//送信元アドレス格納領域とサイズ(受信)
	sockaddr_in			fromAddr;
	int					fromlen;

	//時間
	time_t				t = time(NULL);

	//ファイル書き込み
	std::ofstream*		logFile;

	//送信先の設定とサイズ
	sockaddr_in			toAddr;
	int					tolen;

	//解放
	bool				winSockReleaseFlg = false;
	int ClosedSock();
}

//WinSock初期化
int WinSock::Initialize(SorCTYPE type)
{
	//フォルダ作成とファイルの初期化していない場合
	if (!fileFlg)
	{
		//フォルダが存在するか確認
		bool isExists = std::experimental::filesystem::exists("./Log");

		//フォルダが存在しない場合作成
		if (!isExists)
		{
			std::experimental::filesystem::create_directory("./Log");
		}

		//ファイルを空にする
		std::ofstream server, client;
		server.open("Log\\ServerLog.txt", std::ofstream::out | std::ofstream::trunc);
		client.open("Log\\ClientLog.txt", std::ofstream::out | std::ofstream::trunc);
		server.close();
		client.close();

		//フラグ切り替え
		fileFlg = true;
	}

	//サーバorクライアント
	ownerType = type;

	//ファイルの設定
	//サーバ
	if (ownerType == SERVER)
	{
		logFile = new std::ofstream("Log\\ServerLog.txt");
	}

	//クライアント
	else if (ownerType == CLIENT)
	{
		logFile = new std::ofstream("Log\\ClientLog.txt");
	}

	//初期化済み
	if (winSockInitFlg)
	{
		//何もしない
		*logFile << "Finished:WSAStartup()\n";
		return SUCCESS;
	}

	//初期化
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != SUCCESS)
	{
		//エラー処理 エラーコード出力
		*logFile << "Error( " << WSAGetLastError() << " ):WSAStartup()\n";
		return F_WSASTARTUP;
	}

	//サーバアドレス
	std::string addr;

	//サーバ設定
	if (ownerType == SERVER)
	{
		/*自身のIPアドレス取得*/
		//ホスト名格納領域
		char lpszHostName[50];

		//ホスト情報
		LPHOSTENT host;

		//格納する変数と名前の長さを指定(自由)
		gethostname(lpszHostName, 50);

		//ホスト名から自身のホスト情報を取得
		host = gethostbyname(lpszHostName);

		//自身のアドレス
		addr =
			std::to_string((BYTE)*(host->h_addr_list[0])) + '.' +
			std::to_string((BYTE)*(host->h_addr_list[0] + 1)) + '.' +
			std::to_string((BYTE)*(host->h_addr_list[0] + 2)) + '.' +
			std::to_string((BYTE)*(host->h_addr_list[0] + 3));

		//アドレスの表示
		printf("サーバIPアドレス:%s \n", addr.c_str());
		*logFile << "サーバIPアドレス:" << addr << "\n";
	}

	//クライアント設定
	else if (ownerType == CLIENT)
	{
		//サーバアドレスの入力
		printf("接続するサーバアドレスを入力してください:");
		std::cin >> addr;
		*logFile << "接続するサーバアドレス:" << addr << "\n";
	}

	//どちらでもない
	else
	{
		//エラー処理 エラーコード出力
		*logFile << "Type cannot be selected!!\n";
		return NOT_EXISTS;
	}

	//初期化成功
	*logFile << "Success:WSAStartup()\n";
	datas.clear();
	winSockInitFlg = true;

	//初期データ格納
	INFO serverData{ inet_addr(addr.c_str()) ,htons(SERVER_PORT) };
	datas.push_back(serverData);

	//ソケット作成
	if (MakeSocket() < SUCCESS)return FAILURE;

	//受信設定
	if (SetBindAddr() < SUCCESS)return FAILURE;

	//成功
	return SUCCESS;
}

//受信(UDP)
int WinSock::RecvUDP(char* recvData, int size, const char* variableMold, const char* variableName)
{
	//受信
	int ret = recvfrom(UDPSock, recvData, size, 0, (sockaddr*)&fromAddr, &fromlen);

	//受信エラー
	if (ret == SOCKET_ERROR)
	{
		//エラーコード取得
		int error = WSAGetLastError();

		//データ受信失敗
		if (error != WSAEWOULDBLOCK)
		{
			//エラー処理 エラーコード出力
			*logFile << "Error( " << WSAGetLastError() << " ):recvfrom()\n";
			return F_UDPRECV;
		}

		//受信データなし(ブロッキング)
		return WSAEWOULDBLOCK;
	}

	//時間設定
	char date[64];
	time_t t = time(NULL);
	strftime(date, sizeof(date), "%Y/%m/%d %a %H:%M:%S", localtime(&t));

	//成功
	printf("Success:recvfrom()\n");

	//書き込み
	*logFile << "--------------------------\n";
	*logFile << "Success:recvfrom()\n";
	*logFile << "Accessed date   :" << date << "\n";
	*logFile << "Accessed address:" << inet_ntoa(fromAddr.sin_addr) << "\n";
	*logFile << "Accessed port   :" << ntohs(fromAddr.sin_port) << "\n";
	*logFile << "Received date   :" << variableMold << " " << variableName << "\n";
	*logFile << "--------------------------\n";

	//IPアドレス格納
	return AddAddr(fromAddr.sin_addr.S_un.S_addr, fromAddr.sin_port);

	//IPアドレスを保存しない場合
	return ret;
}

//受信(TCP)
int WinSock::RecvTCP(char* recvData, int size, const char* variableMold, const char* variableName)
{
	return F_TCPRECV;
}

//アドレスとポート設定
void WinSock::SetSendAddr(INFO info)
{
	//初期化
	memset(&toAddr, 0, sizeof(toAddr));

	//アドレスファイミリのセット
	toAddr.sin_family = AF_INET;

	//IPアドレスのセット
	toAddr.sin_addr.S_un.S_addr = info.IP_ADDR;

	//ポート番号のセット
	toAddr.sin_port = info.PORT;

	//送信先データのサイズ取得
	tolen = sizeof(toAddr);
}

//送信(UDP)
int WinSock::SendUDP(char* sendData, int size)
{
	//送信
	int ret = sendto(UDPSock, sendData, size, 0, (sockaddr*)&toAddr, tolen);

	//送信エラー
	if (ret == SOCKET_ERROR)
	{
		//エラー処理 エラーコード出力
		*logFile << "Error( " << WSAGetLastError() << " ):sendto()\n";
		return F_UDPSEND;
	}

	//成功
	printf("Success:sendto()\n");
	*logFile << "Success:sendto()\n";
	return ret;
}

//送信(TCP)
int WinSock::SendTCP(char* sendData, int size)
{
	return F_TCPSEND;
}

//解放
int WinSock::Release()
{
	//解放済み
	if (winSockReleaseFlg)
	{
		//何もしない
		*logFile << "Finished:WSACleanup()\n";
		return SUCCESS;
	}

	//ソケット解放
	ClosedSock();

	//ファイル開放
	logFile->close();
	//delete logFile;

	//終了処理
	if (WSACleanup() != SUCCESS)
	{
		//エラー処理 エラーコード出力
		*logFile << "Error( " << WSAGetLastError() << " ):WSACleanup()\n";
		return F_RELEASE;
	}

	//成功
	*logFile << "Success:WSACleanup()\n";
	datas.clear();
	winSockReleaseFlg = true;

	return SUCCESS;
}

//受信時新規アドレスならベクターに格納
int WinSock::AddAddr(ULONG recvAddr, USHORT recvPort)
{
	//格納チェック
	for (UINT i = 0; i < datas.size(); i++)
	{
		//既に格納されている場合
		if (datas[i].IP_ADDR == recvAddr && datas[i].PORT == recvPort)
		{
			//その番号返還
			return i;
		}
	}

	//末尾に受信アドレスを追加
	INFO pushData = { recvAddr, recvPort };
	datas.push_back(pushData);

	//格納番号返還
	return (int)datas.size() - 1;
}

//ソケット作成
int WinSock::MakeSocket()
{
	//UDP
	UDPSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	//TCP
	//TCPSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_TCP);

	//失敗
	if (UDPSock == INVALID_SOCKET)
	{
		//エラー処理 エラーコード出力
		*logFile << "Error( " << WSAGetLastError() << " ):socket()\n";
		WSACleanup();
		return F_UDPSOCK;
	}

	//失敗
	//if (TCPSock == INVALID_SOCKET)
	//{
	//	//エラー処理 エラーコード出力
	//	printf("Error( %d ):socket()\n", WSAGetLastError());
	//	WSACleanup();
	//	return FAILURE;
	//}

	//成功
	return SUCCESS;
}

//受信設定
int WinSock::SetBindAddr()
{
	//サーバ設定
	if (ownerType == SERVER)
	{
		//初期化
		memset(&bindAddr, 0, sizeof(bindAddr));

		//アドレスファミリのセット
		bindAddr.sin_family = AF_INET;

		//IPアドレスのセット(0.0.0.0に設定することで誰からのアクセスも許可)
		bindAddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);

		//ポート番号のセット
		bindAddr.sin_port = datas[SERVER_DATA].PORT;

		//設定
		if (bind(UDPSock, (sockaddr*)&bindAddr, sizeof(bindAddr)) == SOCKET_ERROR)
		{
			//エラー処理 エラーコード出力
			*logFile << "Error( " << WSAGetLastError() << " ):bind()\n";
			closesocket(UDPSock);
			WSACleanup();
			return F_UDPBIND;
		}

		//成功
		*logFile << "Success:bind()\n";
	}

	//サイズ設定
	fromlen = sizeof(fromAddr);

	//成功
	return SUCCESS;
}

//ソケット解放
int WinSock::ClosedSock()
{
	//終了処理
	if (closesocket(UDPSock) == SOCKET_ERROR)
	{
		//エラー処理 エラーコード出力
		*logFile << "Error( " << WSAGetLastError() << " ):closesocket()\n";
		return F_CLOSE;
	}

	//終了処理
	//if (closesocket(TCPSock) == SOCKET_ERROR)
	//{
	//	//エラー処理 エラーコード出力
	//	printf("Error( %d ):closesocket()\n", WSAGetLastError());
	//	return FAILURE;
	//}

	//成功
	*logFile << "Success:closesocket()\n";
	return SUCCESS;
}

//ブロッキングモードに設定
int WinSock::SetBlockingMode()
{
	//ブロッキングモード
	arg = BLOCK;

	//設定
	if (ioctlsocket(UDPSock, FIONBIO, &arg) == SOCKET_ERROR)
	{
		//エラー処理 エラーコード出力
		*logFile << "Error( " << WSAGetLastError() << " ):ioctlsocket()\n";
		closesocket(UDPSock);
		WSACleanup();
		return F_BLOCK;
	}

	//成功
	*logFile << "Success:ioctlsocket()\n";
	return SUCCESS;
}

//ノンブロッキングモードに設定
int WinSock::SetNonBlockingMode()
{
	//ノンブロッキングモード
	arg = NON_BLOCK;

	//設定
	if (ioctlsocket(UDPSock, FIONBIO, &arg) == SOCKET_ERROR)
	{
		//エラー処理 エラーコード出力
		*logFile << "Error( " << WSAGetLastError() << " ):ioctlsocket()\n";
		closesocket(UDPSock);
		WSACleanup();
		return F_NONBLOCK;
	}

	//成功
	*logFile << "Success:ioctlsocket()\n";
	return SUCCESS;
}
//インクルード
#include "Global.h"
#include <iomanip>


//名前空間
using namespace WinSock;
using namespace std;

//受信データ(キャラクタ構造体)
struct	CharaStatus
{
	//メンバ
	char  charaName[64];
	u_int charaHp;
	u_int charaMp;
	u_int charaAttack;
	u_int charaDefence;
	bool  alive;
}character_;


//main
int main()
{
	//WinSock初期化
	if (Initialize(SERVER) != SUCCESS)return FAILURE;

	int ret1;
	int cnt = 5;
	while (cnt--)
	{
		Sleep(1000);

		//受信
		ret1 = InputData(UDP, VARIABLE(character_));
		if (ret1 != F_UDPRECV && ret1 != WSAEWOULDBLOCK)
		{
			//送信
			if (OutputData(UDP, character_, datas[ret1]) == F_UDPSEND)return FAILURE;
		}
	}

	//終了処理
	if (Release() == F_RELEASE)return FAILURE;
	system("pause");
	return 0;
}
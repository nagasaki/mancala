#include "Client.h"



Client::Client():
	Client(BROADCAST,DEFAULT_SERVER_PORT)
{
}

Client::Client(const string serverIPAddr, const u_short serverPort):
	SERVER_PORT_(serverPort)
{
	//文字サイズ取得
	int size = serverIPAddr.size();

	//動的確保
	SERVER_IP_ADDR_ = new char[size];

	//文字列コピー
	memcpy(SERVER_IP_ADDR_, serverIPAddr.c_str(), size);



}


HRESULT Client::InitClient()
{
	//WinSock2.2初期化
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		cout << "Error:WSAStartup()\n";
		return E_FAIL;

	}
	cout << "Success:WSAStartup()\n";

	//UDPソケット作成
	sock_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock_ < 0)
	{
		cout << "Error:socket()\n";
		WSACleanup();
		return E_FAIL;
	}
	cout << "Success:socket()\n";

	////アドレス設定
	//sockaddr_in toAddr;
	////アドレスの長さ
	//int tolen;
	//memset(&toAddr, 0, sizeof(toAddr));
	//toAddr.sin_family = AF_INET;	//UDP　OR　TCP
	//toAddr.sin_addr.S_un.S_addr = inet_addr(SERVER_IP.c_str());//送信先アドレスの設定
	//toAddr.sin_port = htons(SERVER_PORT_);	//送信先ポート番号
	//tolen = sizeof(toAddr);	//アドレスの長さ

}


//void Client::Send()
//{
//	//送信部。。。無限ループ
//	while (true)
//	{
//		//メッセージ設定
//		string message;
//		cout << "input message:";
//		cin >> message;
//
//		//送信メッセージ生成　ユーザ名：メッセージ
//		string sendMessage = userName + ":" + message;
//
//
//
//
//
//
//
//		/*if (Send(sock, charaStruct, toAddr, tolen))
//		{
//			break;
//		}*/
//
//
//		if (Send(sock, sendMessage, toAddr, tolen))
//		{
//			break;
//		}
//
//		/*if (Send(sock, value, toAddr, tolen))
//		{
//			break;
//		}*/
//
//		////送信
//		//int ret = sendto(sock, sendMessage.c_str(), sendMessage.size(), 0,
//		//	(sockaddr*)&toAddr, tolen);
//
//
//
//
//		////送信エラー
//		//if (ret < 0)
//		//{
//		//	cout << "Error:sendto()\n";
//		//}
//		//else
//		//{
//		//	cout << "Success:sendto()\n";
//		//}
//
//	}
//
//}

BOOL Client::Send(CharaStruct charaStruct)
{


	


	//引数のデータをコピーして
	//送信用のデータを用意する
	CharaStruct sendData;
	{
		//文字列コピー
		//コピー先
		//コピー元
		strcpy_s(sendData.charName, charaStruct.charName);

		//整数値などのコピー
		sendData.charHp = charaStruct.charHp;
		sendData.charMp = charaStruct.charMp;
		sendData.charAttack = charaStruct.charAttack;
		sendData.charDeffence = charaStruct.charDeffence;
		sendData.alive = charaStruct.alive;

	
	}




	//アドレス設定
	sockaddr_in toAddr;
	//アドレスの長さ
	int tolen;
	memset(&toAddr, 0, sizeof(toAddr));
	toAddr.sin_family = AF_INET;	//UDP　OR　TCP
	toAddr.sin_addr.S_un.S_addr = inet_addr(SERVER_IP.c_str());//送信先アドレスの設定
	toAddr.sin_port = htons(SERVER_PORT_);	//送信先ポート番号
	tolen = sizeof(toAddr);	//アドレスの長さ



	//アドレス情報の初期化
	//GetToAddres(&toAddr, &tolen);

		//引数データをネットワークバイトオーダーに変換
		//だが、この時、変換を行うのは、
		//2バイト以上のデータのみ。

		//変換用のデータ
		//CharaStruct sendData;
		//sendData.charHp = charaStruct.charHp;

		////文字列のコピー
		//	//　＝　の式だとコピーがエラーを起こすので注意
		//strcpy_s(charaStruct.charName, sendData.charName);





	//構造体すべてをネットワークバイトーだーには変換しない
	//構造体のメンバ変数の中で、2バイト以上のメンバ変数を変換処理をする。
		{
			//キャラクタHPをネットワークバイトオーダーに変換
				// htonl = long型でホストバイトオーダーをネットワークバイトオーダーに変換
			sendData.charHp = htonl(sendData.charHp);

			sendData.charMp = htonl(sendData.charMp);
			sendData.charAttack = htonl(sendData.charAttack);
			sendData.charDeffence = htonl(sendData.charDeffence);

		}

		//送信（送信データすべて）
		//送信できた場合、戻値に、送ったデータのサイズが帰ってくる
		int ret = sendto(sock_, (char*)&sendData, sizeof(sendData),
			0, (sockaddr*)&toAddr, tolen);



		//送信関数の戻り値である、
		//送信完了したデータサイズ　＝＝　送信したかったデータサイズ
			//真：送信完了
			//偽：送信未完了
		return ret == sizeof(sendData);

	
}

BOOL Client::Send(string message)
{
	//送信用のデータ用意
		//→いらない
		//→なぜならバイトオーダー変換しないから

	////送信データのサイズの取得
	//const int size = message.size();

	////文字列を格納するバッファーを用意
	//	//上記で取得した送信データのサイズ分確保
	//	//変数で配列要素を確保するには、動的確保するしかないので、
	//char* sendData = new char[size];



	//アドレス設定
	sockaddr_in toAddr;
	//アドレスの長さ
	int tolen;


	//アドレス情報の初期化
	GetToAddres(&toAddr, &tolen);

	//string＝1バイトの配列
		//=　ネットワークバイトオーダーに変更無し


	//送信
	//第２引数：char*　型だが、stringはchar*型なので、c_str()にて、メッセージのバッファーのポインタを示す
	//第３引数：文字列のサイズ（stringをsizeofしても、stringはポインタなので、文字列のサイズ自体は出てこない？）
	int ret = sendto(sock_, message.c_str(), message.size(), 0,
		(sockaddr*)&toAddr, tolen);



	////送信用データの
	////解放
	//delete[] sendData;

	//送信関数の戻り値である、
	//送信完了したデータサイズ　＝＝　送信したかったデータサイズ
		//真：送信完了
		//偽：送信未完了
	return ret == message.size();

}
BOOL Client::Send(int message)
{
	//送信データの確保
	int sendData = message;


	//アドレス設定
	sockaddr_in toAddr;
	//アドレスの長さ
	int tolen;



	//アドレス情報の初期化
	GetToAddres(&toAddr, &tolen);

	//ネットワークバイトオーダーに変換
	sendData = htonl(sendData);

	//送信
	//第２引数：char*　型だが、stringはchar*型なので、c_str()にて、メッセージのバッファーのポインタを示す
	//第３引数：文字列のサイズ（stringをsizeofしても、stringはポインタなので、文字列のサイズ自体は出てこない？）
	int ret = sendto(sock_, (char*)&sendData, sizeof(sendData), 0,
		(sockaddr*)&toAddr, tolen);



	//送信関数の戻り値である、
	//送信完了したデータサイズ　＝＝　送信したかったデータサイズ
		//真：送信完了
		//偽：送信未完了
	return ret == sizeof(sendData);
}

void Client::Release()
{
	delete[] SERVER_IP_ADDR_;

	//終了処理
	closesocket(sock_);
	cout << "Success:closesocket()\n";
	WSACleanup();
	cout << "Success:WSACleanup()\n";
}


void Client::GetToAddres(sockaddr_in * toAddr, int * tolen)
{
	//送信時設定
	memset(toAddr, 0, sizeof(&toAddr));
	(*toAddr).sin_family = AF_INET;	//UDP　OR　TCP
	(*toAddr).sin_addr.S_un.S_addr = inet_addr(SERVER_IP.c_str());//送信先アドレスの設定
	(*toAddr).sin_port = htons(SERVER_PORT_);	//送信先ポート番号
	(*tolen) = sizeof(&toAddr);	//アドレスの長さ

}

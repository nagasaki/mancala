#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <WinSock2.h>
#include <iostream>
#include <string>
#include "data.h"

//リンカ
#pragma comment( lib,"ws2_32.lib")




using namespace std;

const string SERVER_IP = "192.168.42.30";

class Client
{
	//ソケット
	//自身のメソッドにてデータの送信を行うときに、使用する
	SOCKET sock_;


	//送信先
	//サーバーのアドレス
	//コンストラクタ生成時に初期化
	char* SERVER_IP_ADDR_;
		/*
		//サーバアドレス
		//https://faq.ricoh.jp/app/answers/detail/a_id/55/~/%E3%83%91%E3%82%BD%E3%82%B3%E3%83%B3%E3%81%AEip%E3%82%A2%E3%83%89%E3%83%AC%E3%82%B9%E3%82%92%E7%A2%BA%E8%AA%8D%E3%81%99%E3%82%8B%E6%96%B9%E6%B3%95%EF%BC%88standard-tcp%2Fip%EF%BC%89/session/L2F2LzEvdGltZS8xNjA2ODgxMjQ4L2dlbi8xNjA2ODgxMjQ4L3NpZC9mVURVa3YwNF9ZRzNwbll3QzglN0ViSVNkSWtabFVyaXUzVDlNRCU3RWFhdEJrS1lLOG56JTdFVGRQcHJNZE5mSldFM3dteXlmTTlxY0lINTlQNGhvNzJSNWFtdEFBRGd6NGl6SlhxbUliVWlhcjZScVNlaFUyYUF5V3Fvd2clMjElMjE=#:~:text=%E3%82%AD%E3%83%BC%E3%83%9C%E3%83%BC%E3%83%89%E3%81%AE%EF%BC%BBWindows%EF%BC%BD%E3%82%AD%E3%83%BC%E3%82%92,%E3%83%91%E3%82%BD%E3%82%B3%E3%83%B3%E3%81%AEIP%E3%82%A2%E3%83%89%E3%83%AC%E3%82%B9%E3%81%A7%E3%81%99%E3%80%82
		//const string SERVERIPADDR = "192.168.44.55";	//405 //今回は、受信する側(サーバー)のアドレス（自分自身になる）
		const string SERVERIPADDR = "192.168.42.24";	//403
		
		*/

	//サーバーポート番号
	//コンストラクタ生成時に初期化
	/*
			//IPアドレスにて送信先をクライアントで決めるが、
		//IPアドレスだけでは、どのプログラムにデータを送るのか識別できないので、
		//どのプログラムにクライアントは送信するか、サーバーは、このポート番号にて受信を許可する　その識別の番号

		送受信は
		（クライアント）IPアドレスと、ポート番号によって送信先を決める
	
	*/
	/*
	
	1024ぐらいまでは使ってはいけないポート番号

	それ以降の番号の中でも、
	サーバーとして使っていい番号がいくらかあって、

	8000番は特に理由はない
	HTTPが８００だから、それ関連でその番号に？

	
	*/
	const u_short SERVER_PORT_;

	//送信先のアドレス設定は、その都度送信時に設定を行えばよい
	//送信先のアドレスを取得
	//引数：送信先アドレス情報を入れる構造体
	//引数：送信先アドレスのサイズ
	void GetToAddres(sockaddr_in* toAddr, int* tolen);





public : 
	//引数なしコンストラクタ
	Client();
	//コンストラクタ
	//引数：サーバーのIPアドレス
	//引数：サーバーのポート番号
	Client(const string serverIPAddr , const u_short serverPort);





	//クライアントの初期化処理
		//ソケットの用意など
	HRESULT InitClient();

	//送信処理
	//charaStatus構造体の送信関数
	//引数データの送信する関数
	//引数：送信データの構造体
	//戻値：送信できたかtrue , false
	BOOL Send(CharaStruct charaStruct);

	//文字列の送信関数
	//引数データの送信する関数
	//引数：送信データ文字列
	//戻値：送信できたかtrue , false
	BOOL Send(string message);

	//整数値の送信関数
	//引数データの送信する関数
	//引数：送信データ値
	//戻値：送信できたかtrue , false
	BOOL Send(int message);

	//終了解放処理
	void Release();


};


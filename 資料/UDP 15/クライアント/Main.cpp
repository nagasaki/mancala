#include <iostream>


#include "data.h"
#include "Client.h"

using namespace std;


int main()
{
	//送信データの宣言
	CharaStruct charaStruct;
	charaStruct.charHp = 500;
	charaStruct.charMp = 200;
	charaStruct.charDeffence = 2;
	charaStruct.charAttack = 55;

	char name[64] = "data";
	//コピー先
	//コピー元
	strcpy_s(charaStruct.charName, name);//文字列コピー
	charaStruct.alive = true;



	//クライアントのインスタンスの生成
	//コンストラクタ引数：送信先IPアドレス
	//コンストラクタ引数：ポート番号
	Client client(DATE_SERVER_403_30, DEFAULT_SERVER_PORT);

	//初期化
	client.InitClient();

	//完全に送信できるまで繰り返す
	while (1)
	{

		//引数のデータは関数先にて崩れない（引数のデータを関数内にて、コピーして、それを送信するようにしている）
		if (client.Send(charaStruct))
		{
			//送信完了
			break;
		
		}

	
	}

	//解放処理
	client.Release();




}
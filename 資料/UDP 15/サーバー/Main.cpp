#include <iostream>

#include "data.h"
#include "Server.h"

using namespace std;


//メイン
int main()
{
	//サーバーのインスタンス生成
	//コンストラクタ引数：サーバーポート番号
	Server server(DEFAULT_SERVER_PORT);

	//サーバーの
	//ソケットの初期化
	server.InitClient();


	
	//受信データをもらう変数
	//構造体
	CharaStruct charaStruct;




	//受信するまで繰り返す
	while(1)
	{
		//構造体データの受信
		if (server.Recv(&charaStruct))
		{
			//受信したら終了
			break;
		}


	}

	//受信データの確認表示
	cout << "キャラ名前：" << charaStruct.charName << "\n";
	cout << "キャラHP：" << charaStruct.charHp << "\n";
	cout << "キャラMP：" << charaStruct.charMp << "\n";
	cout << "キャラATK：" << charaStruct.charAttack << "\n";
	cout << "キャラDEF：" << charaStruct.charDeffence << "\n";
	cout << "生きているか：" << charaStruct.alive << "\n";


	//ソケットの解放
	server.Release();





}
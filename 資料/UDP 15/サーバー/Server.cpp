#include "Server.h"

Server::Server():
	Server(DEFAULT_SERVER_PORT)
{
}

Server::Server(const u_short SERVER_PORT):
	SERVER_PORT_(SERVER_PORT)
{
}

HRESULT Server::InitClient()
{
	// WinSock2.2初期化
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		cout << "Error:WSAStartup()\n";
		return E_FAIL;
	}
	cout << "Success:WSAStartup()\n";


	// UDPソケット作成
	sock_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock_ < 0)
	{
		cout << "Error:socket()\n";
		WSACleanup();
		return E_FAIL;
	}
	cout << "Success:socket()\n";

	// ポート番号割り当て
	sockaddr_in bindAddr;
	memset(&bindAddr, 0, sizeof(bindAddr));
	bindAddr.sin_family = AF_INET;
	bindAddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	bindAddr.sin_port = htons(SERVER_PORT_);

	if (bind(sock_, (sockaddr*)&bindAddr, sizeof(bindAddr)) != 0)
	{
		cout << "Error:bind()\n";
		closesocket(sock_);
		WSACleanup();
		return E_FAIL;
	}
	cout << "Success:bind()\n";




	return S_OK;
}

void Server::Release()
{
	// 終了処理
	closesocket(sock_);
	cout << "Success:closesocket()\n";
	WSACleanup();
	cout << "Success:WSACleanup()\n";

}

BOOL Server::Recv(CharaStruct* charaStruct)
{
	//受信データを入れる変数
	//直接引数のポインタにデータを入れるわけにはいかない（完全にすべてのデータを受け取るとは限らない）
	CharaStruct recvData;


	//受信部
	sockaddr_in fromAddr;				// 送信元アドレス格納領域
	int fromlen = sizeof(fromAddr);		// fromAddrのサイズ

	// 受信
	//ブロッキング関数
	//recvDataに、受信データを受けとる（成功時も、失敗時も）（成功も失敗もしない待ちの時はブロッキングする）
	
	//戻値
	//成功＝　受信したデータのサイズ
	//失敗＝　−１
	int ret = recvfrom(sock_, (char*)&recvData, sizeof(recvData), 0,
		(sockaddr*)&fromAddr, &fromlen);

	



	//受信が完了していたら、（受信できたデータのサイズ＝＝受信しなくてはいけないデータのサイズ）
	//データをポインタにコピー
	if (ret == sizeof(recvData))
	{
		//文字列のコピー
		//　＝　の式だとコピーがエラーを起こすので注意
		//コピー先
		//コピー元
		strcpy_s(charaStruct->charName, recvData.charName);

		//2バイト以上のデータのバイトオーダーを変換する
			//ntohl
			//ネットワークバイトオーダーからホストバイトオーダーへ
		charaStruct->charHp = ntohl(recvData.charHp);
		charaStruct->charMp = ntohl(recvData.charMp);
		charaStruct->charAttack = ntohl(recvData.charAttack);
		charaStruct->charDeffence = ntohl(recvData.charDeffence);
		charaStruct->alive = recvData.alive;
		return true;
	}

	return false;


	//表示確認
	/*
	cout << "キャラ名前：" << charaStruct.charName << "\n";
	cout << "キャラHP：" << charaStruct.charHp << "\n";
	cout << "キャラMP：" << charaStruct.charMp << "\n";
	cout << "キャラATK：" << charaStruct.charAttack << "\n";
	cout << "キャラDEF：" << charaStruct.charDeffence << "\n";
	cout << "生きているか：" << charaStruct.alive << "\n";
	*/

}

BOOL Server::Recv(char str[])
{
	//受信データを入れる変数
	//直接引数のポインタにデータを入れるわけにはいかない（完全にすべてのデータを受け取るとは限らない）

	//char配列で保存先のバッファを作成
		//送られてくるサイズよりも多い、サイズを確保する
		//受け取りを文字列型にすると、バッファサイズがからの０バイトになってしまうので、
		//受け取りのサイズを０バイト分しか受け取れない。＝何も受け取らないということになる。
	char buff[1024];




	//受信部
	sockaddr_in fromAddr;				// 送信元アドレス格納領域
	int fromlen = sizeof(fromAddr);		// fromAddrのサイズ

	// 受信
	//recvDataに、受信データを受けとる
		//配列の添え字なしの変数は配列の先頭アドレスを示す
		//つまりアドレスを引数先でポインタで受け取ることが可能
	int ret = recvfrom(sock_, buff, sizeof(buff), 0,
		(sockaddr*)&fromAddr, &fromlen);



	//受信が完了していたら、（受信できたデータのサイズ＝＝受信しなくてはいけないデータのサイズ）
	//データをポインタにコピー
	if (ret > 0)
	{
		//文字列のコピーは＝で行えない

		//メモリコピー
		//コピー先アドレス（配列の要素なし＝配列の先頭アドレス）
		//コピー元アドレス
		//コピーサイズ
		memcpy(str, buff, sizeof(buff));

		return true;
	}

	return false;

}

BOOL Server::Recv(int * message)
{

	//受信データを入れる変数
	//直接引数のポインタにデータを入れるわけにはいかない（完全にすべてのデータを受け取るとは限らない）
	int recvData = 0;




	//受信部
	sockaddr_in fromAddr;				// 送信元アドレス格納領域
	int fromlen = sizeof(fromAddr);		// fromAddrのサイズ

	// 受信
	//recvDataに、受信データを受けとる
	//第二引数：データを格納するアドレス（受け取ったデータの格納先）
	int ret = recvfrom(sock_, (char*)&recvData, sizeof(recvData), 0,
		(sockaddr*)&fromAddr, &fromlen);



	//受信が完了していたら、（受信できたデータのサイズ＝＝受信しなくてはいけないデータのサイズ）
	//データを引数ポインタにコピー
	if (ret > 0)
	{
		//ホストバイトオーダーに変換
			//int 4バイト　＞　2バイト
		recvData = ntohl(recvData);

		*message = recvData;

		return true;
	}

	return false;

}



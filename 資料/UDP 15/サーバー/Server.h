#pragma once
#include <WinSock2.h>
#include <iostream>
#include <string>

#include "data.h"

#pragma comment( lib, "ws2_32.lib" )


#define _WINSOCK_DEPRECATED_NO_WARNINGS


using namespace std;


class Server
{
private : 
	//ソケット
	SOCKET sock_;

	//サーバーポート番号
		//IPアドレスにて送信先をクライアントで決めるが、
		//IPアドレスだけでは、どのプログラムにデータを送るのか識別できないので、
		//どのプログラムにクライアントは送信するか、サーバーは、このポート番号にて受信を許可する　その識別の番号
	const u_short SERVER_PORT_;


public : 
	//引数なしコンストラクタ
	Server();
	//引数ありコンストラクタ
	//引数：サーバーポート番号
	Server(const u_short SERVERPORT);


	//ソケットの初期化
	HRESULT InitClient();



	//終了解放処理
	void Release();


	//受信関数
	//CharaStruct型　受信関数
	//受け取ったデータを関数呼び出し側でも欲しいと思うので、ポインタで受け取り
	//引数：受信データの型ポインタ（受信データを入れる）
	BOOL Recv(CharaStruct* charaStruct);

	//文字列型　受信関数
	//引数：受信データの型ポインタ（受信データを入れる）
			//引数に配列の要素数なしを置くことで、配列の先頭アドレスが入るので、
			//それをポインタとして受け取る。　参照するときは[]で要素数を指定すればアドレスから、コピーではなく、元のデータにアクセスできる
	BOOL Recv(char str[]);

	//整数型　受信関数
	//引数：受信データの型ポインタ（受信データを入れる）
	BOOL Recv(int* message);




};


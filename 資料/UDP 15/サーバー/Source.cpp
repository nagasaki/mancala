////サーバー側
//
//
//#define _WINSOCK_DEPRECATED_NO_WARNINGS
//
//
//
//#include <WinSock2.h>
//#include <iostream>
//#include <string>
//
//#pragma comment( lib, "ws2_32.lib" )
//
//using namespace std;
//
//const u_short SERVERPORT = 8000;
//
//
////受信データの定義
////受信データの構造体（クライアント側から送られてくるデータ）
//struct CharaStruct
//{
//	char charName[64];	//キャラ名	//これはchar1バイトが配列になっているだけなので、バイト変換の必要なし
//	u_int charHp;		//HP
//	u_int charMp;		//MP
//	u_int charAttack;		//攻撃力
//	u_int charDeffence;		//防御力
//	bool alive;				//生きているか	//1バイト
//
//};
//
//
////受信関数
//	//受け取ったデータを関数呼び出し側でも欲しいと思うので、ポインタで受け取り
//BOOL Recv(SOCKET sock, CharaStruct* charaStruct);
//
////文字列型　受信関数
//BOOL Recv(SOCKET sock, char str[]);
//
////整数型　受信関数
//BOOL Recv(SOCKET sock, int* message);
//
//
//int main()
//{
//	// WinSock2.2初期化
//	WSADATA wsaData;
//	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
//	{
//		cout << "Error:WSAStartup()\n";
//		return -1;
//	}
//	cout << "Success:WSAStartup()\n";
//
//
//	// UDPソケット作成
//	int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
//	if (sock < 0)
//	{
//		cout << "Error:socket()\n";
//		WSACleanup();
//		return -1;
//	}
//	cout << "Success:socket()\n";
//
//	// ポート番号割り当て
//	sockaddr_in bindAddr;
//	memset(&bindAddr, 0, sizeof(bindAddr));
//	bindAddr.sin_family = AF_INET;
//	bindAddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
//	bindAddr.sin_port = htons(SERVERPORT);
//
//	if (bind(sock, (sockaddr*)&bindAddr, sizeof(bindAddr)) != 0)
//	{
//		cout << "Error:bind()\n";
//		closesocket(sock);
//		WSACleanup();
//		return -1;
//	}
//	cout << "Success:bind()\n";
//
//
//
//	CharaStruct charaStruct;
//
//	//文字列を受け取るバッファー
//	//文字列を文字列型で受け取るには無理がある（現段階の私では）
//		//文字配列でバッファーサイズを受け取り、そのバッファーサイズ内に、受け取った文字を入れるという受信の仕方
//	char str[1024];
//
//	int value;
//
//
//	// 受信部...無限ループ
//	while (true)
//	{
//		////受信関数
//		//if (Recv(sock, &charaStruct))
//		//{
//
//		//	break;
//		//}
//
//
//		//受信関数
//		if (Recv(sock, str))
//		{
//			break;
//		}
//
//		//受信関数
//		/*if (Recv(sock, &value))
//		{
//			break;
//		}*/
//	}
//
//
//
//	cout << "キャラ名前：" << charaStruct.charName << "\n";
//	cout << "キャラHP：" << charaStruct.charHp << "\n";
//	cout << "キャラMP：" << charaStruct.charMp << "\n";
//	cout << "キャラATK：" << charaStruct.charAttack << "\n";
//	cout << "キャラDEF：" << charaStruct.charDeffence << "\n";
//	cout << "生きているか：" << charaStruct.alive << "\n";
//
//
//	// 終了処理
//	closesocket(sock);
//	cout << "Success:closesocket()\n";
//	WSACleanup();
//	cout << "Success:WSACleanup()\n";
//
//	return 0;
//}
//
//BOOL Recv(SOCKET sock, CharaStruct * charaStruct)
//{
//
//
//	//受信データを入れる変数
//		//直接引数のポインタにデータを入れるわけにはいかない（完全にすべてのデータを受け取るとは限らない）
//	CharaStruct recvData;
//
//
//	//受信部
//	sockaddr_in fromAddr;				// 送信元アドレス格納領域
//	int fromlen = sizeof(fromAddr);		// fromAddrのサイズ
//
//	// 受信
//	//ブロッキング関数
//	//recvDataに、受信データを受けとる
//	int ret = recvfrom(sock, (char*)&recvData, sizeof(recvData), 0, 
//		(sockaddr*)&fromAddr, &fromlen);
//
//
//
//
//
//	//受信が完了していたら、（受信できたデータのサイズ＝＝受信しなくてはいけないデータのサイズ）
//	//データをポインタにコピー
//	if (ret == sizeof(recvData))
//	{
//		//文字列のコピー
//		//　＝　の式だとコピーがエラーを起こすので注意
//		//コピー先
//		//コピー元
//		strcpy_s(charaStruct->charName , recvData.charName);
//
//		charaStruct->charHp = ntohl(recvData.charHp);
//		charaStruct->charMp = ntohl(recvData.charMp);
//		charaStruct->charAttack = ntohl(recvData.charAttack);
//		charaStruct->charDeffence = ntohl(recvData.charDeffence);
//		charaStruct->alive = recvData.alive;
//		return true;
//	}
//
//	return false;
//
//	
//}
//
//BOOL Recv(SOCKET sock, char str[])
//{
//	//受信データを入れる変数
//	//直接引数のポインタにデータを入れるわけにはいかない（完全にすべてのデータを受け取るとは限らない）
//
//	//char配列で保存先のバッファを作成
//		//送られてくるサイズよりも多い、サイズを確保する
//	//受け取りを文字列型にすると、バッファサイズがからの０バイトになってしまうので、
//		//受け取りのサイズを０バイト分しか受け取れない。＝何も受け取らないということになる。
//	char buff[1024];
//
//
//
//
//	//受信部
//	sockaddr_in fromAddr;				// 送信元アドレス格納領域
//	int fromlen = sizeof(fromAddr);		// fromAddrのサイズ
//
//	// 受信
//	
//	//recvDataに、受信データを受けとる
//
//	//int ret = recvfrom(sock, (char*)&recvData, recvData.size(), 0,
//	//	(sockaddr*)&fromAddr, &fromlen);
//
//	//配列の添え字なしの変数は配列の先頭アドレスを示す
//	
//	//つまりアドレスを引数先でポインタで受け取ることが可能
//	int ret = recvfrom(sock, buff, sizeof(buff), 0,
//		(sockaddr*)&fromAddr, &fromlen);
//
//
//
//	//受信が完了していたら、（受信できたデータのサイズ＝＝受信しなくてはいけないデータのサイズ）
//	//データをポインタにコピー
//	if (ret > 0)
//	{
//		//文字列のコピーは＝で行えない
//		
//		//コピー先
//		//コピー元
//		//コピーサイズ
//		memcpy(str, buff, sizeof(buff));
//
//
//		return true;
//	}
//
//	return false;
//}
//
////OK
//BOOL Recv(SOCKET sock, int * message)
//{
//
//	//受信データを入れる変数
////直接引数のポインタにデータを入れるわけにはいかない（完全にすべてのデータを受け取るとは限らない）
//	int recvData = 0;
//
//
//
//
//	//受信部
//	sockaddr_in fromAddr;				// 送信元アドレス格納領域
//	int fromlen = sizeof(fromAddr);		// fromAddrのサイズ
//
//	// 受信
//
//	//recvDataに、受信データを受けとる
//
//	//第二引数：データを格納するアドレス（受け取ったデータの格納先）
//				//stringの場合、
//	//int ret = recvfrom(sock, (char*)&recvData, recvData.size(), 0,
//	//	(sockaddr*)&fromAddr, &fromlen);
//
//	//配列の添え字なしの変数は配列の先頭アドレスを示す
//	int ret = recvfrom(sock, (char*)&recvData, sizeof(recvData), 0,
//		(sockaddr*)&fromAddr, &fromlen);
//
//
//
//	//受信が完了していたら、（受信できたデータのサイズ＝＝受信しなくてはいけないデータのサイズ）
//	//データをポインタにコピー
//	if (ret > 0)
//	{
//		//ホストバイトオーダーに変換
//		recvData = ntohl(recvData);
//
//		*message = recvData;
//
//		return true;
//	}
//
//	return false;
//
//	
//}
//
//
////バイトオーダーをホストバイトオーダーに変換する処理
////引数のデータは一つのメンバ変数に限る
//	//構造体や、クラスなどは対象外である。
//template <class T>
//BOOL ChangeNtoHl(T* changeMessage)
//{
//	int size = (sizeof(*changeMessage));
//
//	//サイズが２バイト以上なら変換
//	if (size >= 2)
//	{
//		*changeMessage = ntohl(*changeMessage);
//	
//		return TRUE;
//	}
//
//	return FALSE;
//}
//

#pragma once
#include <WinSock2.h>


//サーバアドレス
		//https://faq.ricoh.jp/app/answers/detail/a_id/55/~/%E3%83%91%E3%82%BD%E3%82%B3%E3%83%B3%E3%81%AEip%E3%82%A2%E3%83%89%E3%83%AC%E3%82%B9%E3%82%92%E7%A2%BA%E8%AA%8D%E3%81%99%E3%82%8B%E6%96%B9%E6%B3%95%EF%BC%88standard-tcp%2Fip%EF%BC%89/session/L2F2LzEvdGltZS8xNjA2ODgxMjQ4L2dlbi8xNjA2ODgxMjQ4L3NpZC9mVURVa3YwNF9ZRzNwbll3QzglN0ViSVNkSWtabFVyaXUzVDlNRCU3RWFhdEJrS1lLOG56JTdFVGRQcHJNZE5mSldFM3dteXlmTTlxY0lINTlQNGhvNzJSNWFtdEFBRGd6NGl6SlhxbUliVWlhcjZScVNlaFUyYUF5V3Fvd2clMjElMjE=#:~:text=%E3%82%AD%E3%83%BC%E3%83%9C%E3%83%BC%E3%83%89%E3%81%AE%EF%BC%BBWindows%EF%BC%BD%E3%82%AD%E3%83%BC%E3%82%92,%E3%83%91%E3%82%BD%E3%82%B3%E3%83%B3%E3%81%AEIP%E3%82%A2%E3%83%89%E3%83%AC%E3%82%B9%E3%81%A7%E3%81%99%E3%80%82
		//const string SERVERIPADDR = "192.168.44.55";	//405 //今回は、受信する側(サーバー)のアドレス（自分自身になる）
		//const string SERVERIPADDR = "192.168.42.24";	//403


//サーバーアドレス
#define BROADCAST "255.255.255.255"
#define DATE_SERVER_403_17 "192.168.42.24"
#define DATE_SERVER_403_30 "192.168.42.30"
#define DATE_SERVER_405_17 "192.168.44.55"

//ポート番号
#define DEFAULT_SERVER_PORT 8000










//送信データの構造体、
//クラスをまとめたヘッダ

//このヘッダをクライアント、サーバーでインクルードして送信するデータの作成、受信するデータの型を設定する


//送信データの定義
//送信データの構造体
struct CharaStruct
{
	char charName[64];	//キャラ名	//これはchar1バイトが配列になっているだけなので、バイト変換の必要なし
	u_int charHp;		//HP
	u_int charMp;		//MP
	u_int charAttack;		//攻撃力
	u_int charDeffence;		//防御力
	bool alive;				//生きているか	//1バイト

};
